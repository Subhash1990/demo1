FROM tomcat:8

WORKDIR /usr/local/tomcat/

COPY ./target/*.war webapp.war

EXPOSE 8080